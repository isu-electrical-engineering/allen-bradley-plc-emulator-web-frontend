from flask import Flask, render_template, send_from_directory
from flask_socketio import SocketIO, emit,send
import socket
import json

HOST = '127.0.0.1'
PORT = 10000

app = Flask(__name__,
            static_url_path='',
            static_folder='web/static',
            template_folder='web/templates')
app.config['SECRET_KEY'] = 'ISU-EE-AB-PLC'
socketio = SocketIO(app)


@app.route('/')
def index():
    SlotCount = 30
    WordCount = 256
    TerminalCount = 16
    return render_template(
        'index.html', slotCount = SlotCount, wordCount = WordCount, terminalCount = TerminalCount)


@app.route('/admin')
def admin():
    return "admin"


@socketio.on('test')
def handle_test(json):
    print('received json: ' + str(json))
    emit('test', {'data':'got it!'})


@socketio.on('plcdata')
def handle_plcdata():
    print('Start connect')
    jsonData = ''
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((HOST,PORT))
        s.sendall('Hello'.encode())
        while 1:
            data = s.recv(1)
            if not data:
                break
            jsonData += data.decode()
    emit('plcdata', json.loads(jsonData))

if __name__ == '__main__':
    socketio.run(app)
