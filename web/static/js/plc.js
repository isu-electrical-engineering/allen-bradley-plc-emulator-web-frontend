var socket = io.connect('http://' + document.domain + ':' + location.port);
socket.on('connect',function(){
   socket.emit('test',{data: 'Connection!'});
   socket.emit('plcdata');
});
socket.on('plcdata',function(msg){
   // console.log(msg);
   //  var json = JSON.parse(msg);
    for(let slot = 0; slot < 31; slot++){
        for(let word = 0; word < 1; word++){
            for(let terminal = 0; terminal < 16; terminal++){
                let outTerm = document.getElementsByName("O-" + slot + "-" + word + "-" + terminal);
                if(outTerm[0]){
                    let val = ((msg["Output"][slot * 30 + word] >>> terminal) & 1).toString();
                    outTerm[0].innerText = val;
                    // if()
                }
               let inTerm = document.getElementsByName("I-" + slot + "-" + word + "-" + terminal);
                if(inTerm[0]){
                    let val = ((msg["Input"][slot * 30 + word] >>> terminal) & 1).toString();
                    inTerm[0].innerText = val;
                    // if()
                }
            }
        }
    }
});